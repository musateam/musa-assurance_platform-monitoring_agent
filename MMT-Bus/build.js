"use strict";

const exec = require('child_process').execSync;
const path = require("path");
const write = require("fs").writeFileSync;
const now      = (new Date());
const BUILD_TIME = (now.getYear() + 1900) + "-" + (now.getMonth() + 1) + "-" + now.getDate();

const PLATFORM = require("os").platform();
const VERSION  = require('./package.json').version;
var GIT_HASH   = "1";
try{
    GIT_HASH = ("" + exec('git log --format="%h" -n 1' )).trim();
}catch( e ){
    console.log("Not found git hash");
}


const debFile   = "mmt-bus_" + VERSION + "_" + GIT_HASH + "_" + BUILD_TIME;
const PWD = path.resolve(".");
const tmpFolder = path.join(PWD, debFile );
const tmpBusFolder = path.join( tmpFolder, "opt", "mmt", "bus" );

//source
exec("mkdir -p " + tmpBusFolder );
exec("cp -r node_modules app.js config.json kafka.js kafka-ca.cert " + tmpBusFolder );

//
const usrBinFolder = path.join( tmpFolder, "usr", "bin" );
exec("mkdir -p "+ usrBinFolder );
const usrBinFile = path.join( usrBinFolder, "mmt-bus"); 
write( usrBinFile, "cd /opt/mmt/bus && node app.js");
exec("chmod +x " + usrBinFile );


if( PLATFORM == "linux" ){
    var distro = exec("python -c \"import platform; print platform.linux_distribution()\"");
    //Ubuntu
    if( distro.indexOf( "Ubuntu" ) != -1 ){
      //create DEBIAN/control file
        console.log("Create deb file");
        exec( "mkdir -p " + path.join( tmpFolder, "DEBIAN" ) );
        write( path.join( tmpFolder, "DEBIAN", "control"),
            "Package: mmt-bus" +
            "\nVersion: " + VERSION +
            "\nDepends: nodejs" +
            "\nSection: base" +
            "\nPriority: standard" +
            "\nArchitecture: all" +
            "\nMaintainer: Montimage <contact@montimage.com>" +
            "\nDescription: MMT-Bus: is a tiny Web server that receives data sending by MMT-Connector via HTTP POST request. It then forwards the data to a Kafka server" +
            "\n  Version id: " + GIT_HASH + ". Build time: " + BUILD_TIME +
            "\nHomepage: http://www.montimage.com\n"
        );
    
        exec("dpkg-deb -b " + debFile );
    }
    else if( distro.indexOf( "CentOS" ) != -1 ){
        //create RPM spec file
          console.log("Create rpm file");
          write( "mmt-bus.spec",
              "Summary:  MMT-BUS" +
              "\nName: mmt-bus" +
              "\nVersion: " + VERSION +
              "\nRelease: " + GIT_HASH +
              "\nLicense: proprietary" +
              "\nGroup: Development/Tools" +
              "\nURL: http://montimage.com/" +
              "\n" +
              "\nBuildRoot: %{_topdir}/BUILD/%{name}-%{version}-%{release}" +
              "\n" +
              "\n%description" +
              "\nMMT-DPI is a library for deep packet inspection." +
              "\nBuild date: " + BUILD_TIME +
              "\n" +
              "\n%prep" +
              "\nrm -rf %{buildroot}" +
              "\nmkdir -p %{buildroot}/" +
              "\nmv "+ tmpFolder +"/*  %{buildroot}/" +
              "\n" +
              "\n%clean" +
              "\nrm -rf %{buildroot}" +
              "\n" +
              "\n%files" +
              "\n%defattr(-,root,root,-)" +
              "\n/opt/mmt/bus/*" +
              "\n/usr/bin/mmt-bus" +
              "\n%post" +
              "\nldconfig"
          );
          const rpmDir = path.join( PWD, "rpmbuild" );
          exec( "mkdir -p "+ rpmDir +"/{RPMS,BUILD}" );
          exec('rpmbuild --quiet --rmspec --define "_topdir '+ rpmDir +'" --define "_rpmfilename ../../'+ debFile +'.rpm" -bb ./mmt-bus.spec');
          exec( "rm -rf " + rpmDir + " " + tmpFolder );
      }else{
          console.log("Donot support this distro: " + distro);
      }
}else if( PLATFORM == "darwin" ){
    console.log("Create tar.gz file");
    exec("tar -czf " + debFile + ".tar.gz " + debFile );
}else 
    console.error("Donot support " + PLATFORM );

exec("rm -rf " + debFile );