This folder contains NodeJS code of MMT-Bus.
MMT-Bus is a tiny Web server that receives data sending by MMT-Connector via HTTP POST request.
It then forwards the data to a Kafka server.

# Requirement

   Nodejs > v4.0

# Run

   node app.js
   
# Package

   node build.js

# Configuration

- `web_server`: parameters of port/host on which MMT-Bus will received input
- `kafka_server`: parameters allowing MMT-Bus to connect to the kafka server
- `topic_name`: name of topic on which MMT-Bus will write data to
- `delay_between_requests`: minimal delay, in millisecond, between 2 consecutive inputs. 
   This avoids DDoS attack when attacker send a lot of input. 
- `is_in_debug_mode`: `true` to print out data received