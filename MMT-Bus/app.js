"use strict";

const http   = require('http');
const queryString = require('querystring');
const config = require('./config.json');
const kafka  = require("./kafka.js").createClient();

const HTTP_CODE = {
        TOO_MANY_REQUESTS: 429,
        OK: 200,
        BAD_REQUEST: 400,
        NOT_FOUND: 404,
        INTERNAL_SERVER_ERROR: 500
}

function now(){
    return (new Date()).getTime(); //milli seconds
}

function setDefault( obj, att, val ){
    if( obj[ att ] == undefined )
        obj[att] = val;
}

setDefault( config, "topic_name", "mmt-bus" );
setDefault( config, "delay_between_requests", 10 );
setDefault( config.web_server, "port", 4567 );
setDefault( config.web_server, "host", "localhost" );

setDefault( config.kafka_server, "port", 2181 );
setDefault( config.kafka_server, "host", "localhost" );

if( ! config.is_in_debug_mode )
    console.info = function(){};

var lastRequestTime = now();
const server = http.createServer( function(req, res) {
    
    //avoid 2 consecutive requests
    const ts = now();
    if( ts - lastRequestTime < config.delay_between_requests ){
        res.writeHead( HTTP_CODE.TOO_MANY_REQUESTS, {'Content-Type': 'text/html'});
        res.end("Too much!");
    }
    lastRequestTime = ts;

    
    if (req.method == 'POST') {
        var body = '';
        req.on('data', function (data) {
            body += data;
        });
        req.on('end', function () {
            var data = "";
            try{
                data = queryString.parse( body );
                if( data == undefined || data.Event == undefined )
                    throw new Error("Not found Event" );
                
                data = new Buffer( data.Event, 'base64')
                data = data.toString();
                console.info( data );
            }catch( e ){
                res.writeHead( HTTP_CODE.BAD_REQUEST, {'Content-Type': 'text/html'});
                res.end( e.message );
            }
            
            kafka.publish( config.topic_name, data, function( err, data){
                if( err ){
                    res.writeHead( HTTP_CODE.INTERNAL_SERVER_ERROR, {'Content-Type': 'application/json'});
                    res.end( JSON.stringify( err ) );
                }else{
                    res.writeHead( HTTP_CODE.OK, {'Content-Type': 'application/json'});
                    res.end( JSON.stringify( data ));
                }
            });
        });
    }
    else{
        res.writeHead( HTTP_CODE.OK, {'Content-Type': 'text/html'});
        res.end( "nothing here" );
    }

});

server.listen( config.web_server.port, config.web_server.host);

console.log('MMT-BUS is listening at http://' + config.web_server.host + ':' + config.web_server.port );
console.log( "Configuration:" + JSON.stringify( config ));
console.log("================================")