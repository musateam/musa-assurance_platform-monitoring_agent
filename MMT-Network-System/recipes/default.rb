#
# Cookbook:: mmt-network
# Recipe:: default
#
# Copyright:: 2017, Montimage EURL, All Rights Reserved.

platform = node[:platform]

#global variable representing name of cookbook
# => we donot need to re-declare
#cookbook_name = "install-mmt-network"

#install mmt packages
bash "install-mmt-packages" do
  user "root"
  cwd "#{Chef::Config[:file_cache_path]}/cookbooks/#{cookbook_name}/files/#{platform}"

  case platform
  when "ubuntu"
    code <<-EOF
      dpkg -i mmt-dpi*.deb
      dpkg -i mmt-security*.deb
      dpkg -i mmt-probe*.deb
      ldconfig
    EOF
  when "centos"
    code <<-EOF
      rpm -i mmt-dpi*.rpm
      rpm -i mmt-security*.rpm
      rpm -i mmt-probe*.rpm
      ldconfig
    EOF
  end
end
